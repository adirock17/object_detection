The project was designed using reference from the following tutorial:
https://pythonprogramming.net/introduction-use-tensorflow-object-detection-api-tutorial/

Few commands used during the process:
.\protoc.exe object_detection/protos/*.proto --python_out=.
tensorboard --logdir=./training/
python train.py --logtostderr --train_dir=.\training\ --pipeline_config_path=.\training\faster_rcnn_resnet101_pets.config
python export_inference_graph.py --input_type image_tensor --pipeline_config_path training/faster_rcnn_resnet101_pets.config --trained_checkpoint_prefix training/model.ckpt-9892 --output_directory output